<?php

namespace Drupal\uw_wcms_gen\Service;

use Drupal\Core\Entity\EntityInterface;

/**
 * UW Node content service interface.
 *
 * @package Drupal\uw_wcms_gen\Service
 */
interface UWNodeContentServiceInterface {

  /**
   * Media fields always have value, and listing and hero share media.
   */
  const MEDIA_REQUIRED_SHARED = 1;

  /**
   * All media fields must have value that is independent.
   */
  const MEDIA_REQUIRED_INDEPENDENT = 2;

  /**
   * Media field depends on random function, shared listing and hero value.
   */
  const MEDIA_OPTIONAL_SHARED = 3;

  /**
   * Each media field is optional and independent from each other.
   */
  const MEDIA_OPTIONAL_INDEPENDENT = 4;

  /**
   * Generate content without media.
   */
  const MEDIA_NOT_USED = 5;

  /**
   * Creates Copy text block.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   Block entity.
   */
  public function createBlockCopyText(): EntityInterface;

  /**
   * Creates full width image block.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   Block entity.
   */
  public function createBlockFullWidthImage(): EntityInterface;

  /**
   * Creates blockquote layout block.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   Block entity.
   */
  public function createBlockBlockquote(): EntityInterface;

  /**
   * Creates random layout builder blocks.
   *
   * @param int $blocks
   *   Number of blocks to generate.
   *
   * @return array
   *   Array of block content.
   */
  public function createRandomBlocks(int $blocks = 3): array;

  /**
   * Creates sections to be used in layout builder.
   *
   * @param string $uuid
   *   UUID of the node.
   * @param int $blocks
   *   Number of blocks to create.
   *
   * @return array
   *   Generated sections with blocks in array.
   */
  public function createSections(string $uuid, int $blocks = 3): array;

  /**
   * Creates values for common fields.
   *
   * @param array $options
   *   Additional options.
   *
   * @return array
   *   Key/value pairs for common node fields.
   */
  public function createCommonFields(array $options = [
    'media' => self::MEDIA_OPTIONAL_SHARED,
    'days-before' => -100,
    'days-after' => 100,
  ]): array;

  /**
   * Generates Blog content type.
   *
   * @param array $options
   *   Additional options.
   * @param bool $published
   *   Default publication state.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   Generated entity.
   */
  public function generateBlog(array $options = [
    'media' => self::MEDIA_OPTIONAL_SHARED,
    'days-before' => -100,
    'days-after' => 100,
  ],
  bool $published = TRUE
  ): EntityInterface;

  /**
   * Generate News item content type.
   *
   * @param array $options
   *   Additional options.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   Generated entity.
   */
  public function generateNewsItem(array $options = [
    'media' => self::MEDIA_OPTIONAL_SHARED,
    'days-before' => -100,
    'days-after' => 100,
  ]): EntityInterface;

  /**
   * Generate Event content type.
   *
   * @param array $options
   *   Additional options.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   Generated entity.
   */
  public function generateEvent(array $options = [
    'media' => self::MEDIA_OPTIONAL_SHARED,
    'days-before' => -100,
    'days-after' => 100,
  ]): EntityInterface;

  /**
   * Generate Web page content type.
   *
   * @param array $options
   *   Additional options.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   Generated entity.
   */
  public function generateWebpage(array $options = [
    'media' => self::MEDIA_OPTIONAL_SHARED,
    'days-before' => -100,
    'days-after' => 100,
  ]): EntityInterface;

  /**
   * Generate Profile content type.
   *
   * @param array|int[] $options
   *   Additional options.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   Generate content.
   */
  public function generateProfile(array $options = [
    'media' => self::MEDIA_OPTIONAL_SHARED,
    'days-before' => -100,
    'days-after' => 100,
  ]): EntityInterface;

  /**
   * Generate Project content type.
   *
   * @param array|int[] $options
   *   Additional options.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   Generate content.
   */
  public function generateProject(array $options = [
    'media' => self::MEDIA_OPTIONAL_SHARED,
    'days-before' => -100,
    'days-after' => 100,
    'blocks' => 3,
  ]): EntityInterface;

  /**
   * Check for a site footer.
   *
   * @return bool
   *   Whether there is a site footer.
   */
  public function checkSiteFooter(): bool;

  /**
   * Generate site footer.
   *
   * @param array|int[] $options
   *   Additional options.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   Generate content.
   */
  public function generateSiteFooter(array $options = [
    'media' => self::MEDIA_OPTIONAL_SHARED,
    'days-before' => -100,
    'days-after' => 100,
    'blocks' => 3,
  ]): EntityInterface;

  /**
   * Generate Service content type.
   *
   * @param array|int[] $options
   *   Additional options.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   Generate content.
   */
  public function generateService(array $options = [
    'media' => self::MEDIA_OPTIONAL_SHARED,
    'days-before' => -100,
    'days-after' => 100,
    'blocks' => 3,
  ]): EntityInterface;

  /**
   * Generate banner images block.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   Block entity.
   *
   * @throws \Exception
   *   General exception from random function.
   */
  public function createBlockBannerImages(): EntityInterface;

  /**
   * Generates Call to action block.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   Block entity.
   */
  public function createBlockCallToAction(): EntityInterface;

  /**
   * Generates Fact and figures block.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   Block entity.
   */
  public function createBlockFactAndFigures(): EntityInterface;

  /**
   * Create Image gallery block.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   Block entity.
   */
  public function createBlockImageGallery(): EntityInterface;

  /**
   * Creates Related links block.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   Block entity.
   */
  public function createBlockRelatedLinks(): EntityInterface;

  /**
   * Generate Catalog item node.
   *
   * @param array $options
   *   Additional options for new content.
   * @param bool $published
   *   Moderation status.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   Newly generated entity.
   */
  public function generateCatalogItem(array $options = [
    'media' => self::MEDIA_OPTIONAL_SHARED,
    'days-before' => -100,
    'days-after' => 100,
    'blocks' => 3,
  ], bool $published = TRUE): EntityInterface;

  /**
   * Generate Opportunity node.
   *
   * @param array $options
   *   Additional options for new content.
   * @param bool $published
   *   Moderation status.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   Newly generated entity.
   */
  public function generateOpportunity(array $options = [
    'media' => self::MEDIA_OPTIONAL_SHARED,
    'days-before' => 0,
    'days-after' => 100,
    'blocks' => 3,
  ], bool $published = TRUE): EntityInterface;

}
