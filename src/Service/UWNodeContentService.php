<?php

namespace Drupal\uw_wcms_gen\Service;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\layout_builder\Section;
use Drupal\layout_builder\SectionComponent;
use Drupal\paragraphs\Entity\Paragraph;

/**
 * UW Node content custom service implementation.
 */
class UWNodeContentService implements UWNodeContentServiceInterface {

  // Predefined text format.
  const TEXT_FORMAT = 'uw_tf_standard';

  // Basic text format.
  const TEXT_FORMAT_BASIC = 'uw_tf_basic';

  /**
   * Entity type manager from Drupal core.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Custom text service from this module.
   *
   * @var \Drupal\uw_wcms_gen\Service\UWTextServiceInterface
   */
  protected UWTextServiceInterface $textService;

  /**
   * Custom media service from this module.
   *
   * @var \Drupal\uw_wcms_gen\Service\UWMediaServiceInterface
   */
  protected UWMediaServiceInterface $mediaService;

  /**
   * Block content storage.
   *
   * @var \Drupal\Core\Entity\Sql\SqlContentEntityStorage
   */
  protected $blockContentStorage;

  /**
   * Constructs new service object.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, UWTextServiceInterface $textService, UWMediaServiceInterface $mediaService) {
    $this->entityTypeManager = $entityTypeManager;
    $this->textService = $textService;
    $this->mediaService = $mediaService;

    $this->blockContentStorage = $this->entityTypeManager->getStorage('block_content');
  }

  /**
   * {@inheritDoc}
   */
  public function createBlockCopyText(): EntityInterface {

    // Tell block entity manager to create a block of type copy text.
    $block = $this->blockContentStorage->create([
      'type' => 'uw_cbl_copy_text',
    ]);

    // Set the text and format fields for text copy.
    $block->field_uw_copy_text->value = $this->textService->randomParagraphs(2);
    $block->field_uw_copy_text->format = self::TEXT_FORMAT;

    // Set the title of the block.
    $block->setInfo($this->textService->randomTitle());
    $block->info->value = $this->textService->randomTitle();

    $block->save();

    // Return the newly created block as entity.
    return $block;
  }

  /**
   * Creates full width image block.
   */
  public function createBlockFullWidthImage(): EntityInterface {

    // Tell block entity manager to create a block of type copy text.
    $block = $this->blockContentStorage->create([
      'type' => 'uw_cbl_image',
    ]);

    // Set the text and format fields for text copy.
    $block->field_uw_image = ['target_id' => $this->mediaService->randomMediaId()];

    // Set the title of the block.
    $block->setInfo($this->textService->randomTitle());
    $block->info->value = $this->textService->randomTitle();

    $block->save();

    // Return the newly created block as entity.
    return $block;
  }

  /**
   * {@inheritDoc}
   */
  public function createBlockBlockquote(): EntityInterface {

    // Tell block entity manager to create a block of type copy text.
    $block = $this->blockContentStorage->create([
      'type' => 'uw_cbl_blockquote',
    ]);

    $block->field_uw_bq_quote_attribution->value = $this->textService->randomName();
    $block->field_uw_bq_quote_attribution->format = self::TEXT_FORMAT;
    $block->field_uw_bq_quote_text->value = $this->textService->randomParagraphs(1);
    $block->field_uw_bq_quote_text->format = self::TEXT_FORMAT;

    // Set the title of the block.
    $block->setInfo($this->textService->randomTitle());
    $block->info->value = $this->textService->randomTitle();

    $block->save();

    // Return the newly created block as entity.
    return $block;
  }

  /**
   * {@inheritDoc}
   */
  public function createRandomBlocks(int $blocks = 3): array {

    // Force first block to be copy text.
    $result[] = [
      'type' => 'inline_block:uw_cbl_copy_text',
      'block' => $this->createBlockCopyText(),
    ];

    // Function names to generate blocks.
    $available_blocks = [
      // Disabling full width image, since this block has moved to code,
      // and we need a different way to create it.
//      'inline_block:uw_cbl_image' => 'createBlockFullWidthImage',
      'inline_block:uw_cbl_copy_text' => 'createBlockCopyText',
      'inline_block:uw_cbl_blockquote' => 'createBlockBlockquote',
      'inline_block:uw_cbl_banner_images' => 'createBlockBannerImages',
      'inline_block:uw_cbl_call_to_action' => 'createBlockCallToAction',
//      'inline_block:uw_cbl_facts_and_figures' => 'createBlockFactAndFigures',
    ];

    // Starting from 1 instead of 0 (zero) since we already have one block.
    for ($i = 1; $i < $blocks; $i++) {
      // This will be only createBlockCopyText for now.
      $key = array_rand($available_blocks);
      $func_name = $available_blocks[$key];

      $result[] = [
        'type' => $key,
        'block' => call_user_func([$this, $func_name]),
      ];
    }

    return $result;
  }

  /**
   * {@inheritDoc}
   */
  public function createSections(string $uuid, int $blocks = 3): array {
    $sections = [];

    $blocks = $this->createRandomBlocks($blocks);

    foreach ($blocks as $block) {
      // Create a new section with UW 1 column layout.
      $section = new Section('uw_1_column');

      // Get the config for the component, which will be
      // the block that we created above.
      $pluginConfiguration = [
        'id' => $block['type'],
        'label' => $this->textService->randomTitle(),
        'provider' => 'block_content',
        'label_display' => (bool) mt_rand(0, 1),
        'view_mode' => 'full',
        'block_revision_id' => $block['block']->id(),
        'block_serialized' => NULL,
        'context_mapping' => [],
      ];

      // Create a new section component using the node and plugin config.
      $component = new SectionComponent(
        $uuid,
        'first',
        $pluginConfiguration,
        [
          'layout_builder_id' => "",
        ]
      );

      // Add the component to the section.
      $section->appendComponent($component);

      // Add the section to the sections array.
      $sections[] = $section;
    }

    $code_blocks = $this->createRandomCodeBlocks($blocks);

    // Add code blocks here.
    foreach ($code_blocks as $block) {
      $block['label'] = $this->textService->randomTitle();
      $block['label_display'] = TRUE;

      // Create a new section with UW 1 column layout.
      $section = new Section('uw_1_column');

      // Create a new section component using the node and plugin config.
      $component = new SectionComponent(
        $uuid,
        'first',
        $block,
        [
          'layout_builder_id' => "",
        ]
      );

      // Add the component to the section.
      $section->appendComponent($component);

      // Add the section to the sections array.
      $sections[] = $section;
    }

    return $sections;
  }

  /**
   * {@inheritDoc}
   */
  public function createCommonFields(array $options = [
    'media' => self::MEDIA_OPTIONAL_SHARED,
    'days-before' => -100,
    'days-after' => 100,
    'blocks' => 3,
  ]): array {
    $fields = [
      'date' => $this->textService->randomDate($options['days-before'], $options['days-after']),
    ];

    // If days before is less than zero (in the past) use it.
    if ($options['days-before'] < 0) {
      $fields['created'] = $this->textService->randomTimestamp($options['days-before'], 0);
    }
    // If asking for future dates only, use -10 days from today as created date.
    else {
      $fields['created'] = $this->textService->randomTimestamp(-10, 0);
    }

    return array_merge_recursive($fields, $this->createMedia($options['media']));
  }

  /**
   * {@inheritDoc}
   */
  public function generateBlog(array $options = [
    'media' => self::MEDIA_OPTIONAL_SHARED,
    'days-before' => -100,
    'days-after' => 100,
    'blocks' => 3,
  ],
  bool $published = TRUE
  ): EntityInterface {

    // Generate common fields used across multiple content types.
    $common = $this->createCommonFields($options);

    $node = $this->entityTypeManager->getStorage('node')->create([
      'type' => 'uw_ct_blog',
      'uid' => 1,
      'title' => $this->textService->randomTitle(),
      'status' => $published,
      'created' => $common['created'],
      'field_uw_blog_summary' => [
        'value' => $this->textService->randomParagraphs(2),
        'format' => self::TEXT_FORMAT,
      ],
      'field_uw_meta_description' => $this->textService->randomTitle(),
      'field_uw_blog_date' => $common['date'],
      'field_uw_blog_listing_page_image' => $common['listing_media'],
      'field_uw_hero_image' => $common['hero_media'],
    ]);

    // Array to store generated sections.
    $sections = $this->createSections($node->uuid(), $options['blocks']);

    // Set the sections.
    $node->layout_builder__layout->setValue($sections);
    if ($published) {
      $node->set('moderation_state', 'published');
    }

    // Save the node.
    $node->save();

    return $node;
  }

  /**
   * {@inheritDoc}
   */
  public function generateNewsItem(array $options = [
    'media' => self::MEDIA_OPTIONAL_SHARED,
    'days-before' => -100,
    'days-after' => 100,
    'blocks' => 3,
  ]): EntityInterface {

    // Generate common fields used across multiple content types.
    $common = $this->createCommonFields($options);

    $node = $this->entityTypeManager->getStorage('node')->create([
      'type' => 'uw_ct_news_item',
      'uid' => 1,
      'title' => $this->textService->randomTitle(),
      'status' => 1,
      'created' => $common['created'],
      'field_uw_news_summary' => [
        'value' => $this->textService->randomParagraphs(2),
        'format' => self::TEXT_FORMAT,
      ],
      'field_uw_meta_description' => $this->textService->randomTitle(),
      'field_uw_news_date' => $common['date'],
      'field_uw_news_listing_page_image' => $common['listing_media'],
      'field_uw_hero_image' => $common['hero_media'],
    ]);

    // Array to store generated sections.
    $sections = $this->createSections($node->uuid(), $options['blocks']);

    // Set the sections.
    $node->layout_builder__layout->setValue($sections);
    $node->set('moderation_state', 'published');

    // Save the node.
    $node->save();

    return $node;
  }

  /**
   * {@inheritDoc}
   */
  public function generateEvent(array $options = [
    'media' => self::MEDIA_OPTIONAL_SHARED,
    'days-before' => -100,
    'days-after' => 100,
    'blocks' => 3,
  ]): EntityInterface {

    // Generate common fields used across multiple content types.
    $common = $this->createCommonFields($options);
    $random_timestamp = $this->textService->randomTimestamp($options['days-before'], $options['days-after']);

    $node = $this->entityTypeManager->getStorage('node')->create([
      'type' => 'uw_ct_event',
      'uid' => 1,
      'title' => $this->textService->randomTitle(),
      'status' => 1,
      'created' => $common['created'],
      'field_uw_event_summary' => [
        'value' => $this->textService->randomParagraphs(2),
        'format' => self::TEXT_FORMAT,
      ],
      'field_uw_meta_description' => $this->textService->randomTitle(),
      'field_uw_event_date' => [
        [
          'value' => $random_timestamp,
          'end_value' => ($random_timestamp + 86340),
          'duration' => '1439',
          'rrule' => NULL,
          'rrule_index' => NULL,
          'timezone' => '',
        ],
      ],
      'field_uw_event_listing_page_img' => $common['listing_media'],
      'field_uw_hero_image' => $common['hero_media'],
      'field_uw_event_cost' => mt_rand(0, 1000),
    ]);

    // Array to store generated sections.
    $sections = $this->createSections($node->uuid(), $options['blocks']);

    // Set the sections.
    $node->layout_builder__layout->setValue($sections);
    $node->set('moderation_state', 'published');

    // Save the node.
    $node->save();

    return $node;
  }

  /**
   * {@inheritDoc}
   */
  public function generateWebpage(array $options = [
    'media' => self::MEDIA_OPTIONAL_SHARED,
    'days-before' => -100,
    'days-after' => 100,
    'blocks' => 3,
  ]): EntityInterface {

    $node = $this->entityTypeManager->getStorage('node')->create([
      'type' => 'uw_ct_web_page',
      'uid' => 1,
      'title' => $this->textService->randomTitle(),
      'status' => 1,
      'created' => $this->textService->randomTimestamp($options['days-before'], 0),
      'field_uw_meta_description' => $this->textService->randomTitle(),
    ]);

    // Array to store generated sections.
    $sections = $this->createSections($node->uuid(), $options['blocks']);

    // Set the sections.
    $node->layout_builder__layout->setValue($sections);
    $node->set('moderation_state', 'published');

    // Save the node.
    $node->save();

    return $node;
  }

  /**
   * Create media for listing and hero fields based on parameter.
   *
   * @param int $option
   *   Media option from drush command.
   *
   * @return array
   *   Array with media values for listing and hero field.
   */
  private function createMedia(int $option): array {
    // Based on option set media for landing and hero fields.
    switch ($option) {
      case UWNodeContentServiceInterface::MEDIA_REQUIRED_SHARED:
        $media['listing_media'] = $this->mediaService->randomMediaId();
        $media['hero_media'] = $media['listing_media'];
        break;

      case UWNodeContentServiceInterface::MEDIA_REQUIRED_INDEPENDENT:
        $media['listing_media'] = $this->mediaService->randomMediaId();
        $media['hero_media'] = $this->mediaService->randomMediaId();
        break;

      case UWNodeContentServiceInterface::MEDIA_OPTIONAL_SHARED:
        $media['listing_media'] = mt_rand(0, 1) ? $this->mediaService->randomMediaId() : '';
        $media['hero_media'] = $media['listing_media'];
        break;

      case UWNodeContentServiceInterface::MEDIA_OPTIONAL_INDEPENDENT:
        $media['listing_media'] = mt_rand(0, 1) ? $this->mediaService->randomMediaId() : '';
        $media['hero_media'] = mt_rand(0, 1) ? $this->mediaService->randomMediaId() : '';
        break;

      case UWNodeContentServiceInterface::MEDIA_NOT_USED:
      default:
        $media['listing_media'] = '';
        $media['hero_media'] = '';
    }

    return $media;
  }

  /**
   * {@inheritDoc}
   */
  public function generateContact(array $options = [
    'media' => self::MEDIA_OPTIONAL_SHARED,
    'blocks' => 3,
  ]): EntityInterface {

    $name = $this->textService->randomName();
    [$first, $last] = explode(' ', $name);
    $email = substr(strtolower($first[0] . $last), 0, 8) . '@uwaterloo.ca';

    $contact = $this->entityTypeManager->getStorage('node')->create([
      'type' => 'uw_ct_contact',
      'uid' => 1,
      'title' => $name,
      'status' => 1,
      'field_uw_meta_description' => 'Meta for contact ' . $name,
      'field_uw_ct_contact_sort_name' => $name,
      'field_uw_ct_contact_affiliation' => 'University of Waterloo',
      'field_uw_ct_contact_title' => $this->textService->randomPosition(),
      'field_uw_ct_contact_email' => $email,
    ]);

    // Array to store generated sections.
    $sections = $this->createSections($contact->uuid(), $options['blocks']);

    // Set the sections.
    $contact->layout_builder__layout->setValue($sections);
    $contact->set('moderation_state', 'published');

    // Save the node.
    $contact->save();

    return $contact;
  }

  /**
   * {@inheritDoc}
   */
  public function generateProfile(array $options = [
    'media' => self::MEDIA_OPTIONAL_SHARED,
    'blocks' => 3,
  ]): EntityInterface {

    $name = $this->textService->randomName();

    $profile = $this->entityTypeManager->getStorage('node')->create([
      'type' => 'uw_ct_profile',
      'uid' => 1,
      'title' => $name,
      'status' => 1,
      'field_uw_meta_description' => 'Meta for profile ' . $name,
      'field_uw_ct_profile_sort_name' => $name,
      'field_uw_ct_profile_title' => $this->textService->randomPosition(),
      'field_uw_profile_summary' => [
        'value' => $this->textService->randomParagraphs(2),
        'format' => self::TEXT_FORMAT,
      ],
    ]);

    // Array to store generated sections.
    $sections = $this->createSections($profile->uuid(), $options['blocks']);

    // Set the sections.
    $profile->layout_builder__layout->setValue($sections);
    $profile->set('moderation_state', 'published');

    // Save the node.
    $profile->save();

    return $profile;
  }

  /**
   * {@inheritDoc}
   */
  public function generateProject(array $options = [
    'media' => self::MEDIA_OPTIONAL_SHARED,
    'days-before' => -100,
    'days-after' => 100,
    'blocks' => 3,
  ]): EntityInterface {

    // Get some of the common fields.
    $common = $this->createCommonFields($options);

    // Get the project status from the taxonomy term.
    $project_states_types = $this->entityTypeManager->getStorage('taxonomy_term')
      ->loadByProperties([
        'vid' => 'uw_vocab_project_status',
      ]);
    $project_status = $project_states_types[array_rand($project_states_types)];

    // Create the new node with at least the required fields.
    $project = $this->entityTypeManager->getStorage('node')->create([
      'type' => 'uw_ct_project',
      'uid' => 1,
      'title' => $this->textService->randomTitle(),
      'status' => 1,
      'field_uw_meta_description' => $this->textService->randomTitle(),
      'field_uw_project_summary' => [
        'value' => $this->textService->randomParagraphs(2),
        'format' => 'uw_tf_basic',
      ],
      'field_uw_project_listing_image' => $common['listing_media'],
      'field_uw_project_status' => $project_status,
    ]);

    // Array to store generated sections.
    $sections = $this->createSections($project->uuid(), $options['blocks']);

    // Set the sections.
    $project->layout_builder__layout->setValue($sections);
    $project->set('moderation_state', 'published');

    // Save the node.
    $project->save();

    return $project;
  }

  /**
   * {@inheritDoc}
   */
  public function checkSiteFooter(): bool {

    // Get any site footer nodes.
    $site_footer = \Drupal::entityTypeManager()->getStorage('node')
      ->loadByProperties(['type' => 'uw_ct_site_footer']);

    // If there is a site footer, set an error and return.
    if (empty($site_footer)) {
      return FALSE;
    }

    return TRUE;
  }

  /**
   * {@inheritDoc}
   */
  public function generateSiteFooter(array $options = [
    'media' => self::MEDIA_OPTIONAL_SHARED,
    'days-before' => -100,
    'days-after' => 100,
    'blocks' => 1,
  ]): EntityInterface {

    // Get a random logo for the site footer.
    $logos = _uw_ct_site_footer_get_allowed_values();
    $logos = array_keys($logos);
    $logo = $logos[array_rand($logos)];

    // Create a site footer node.
    $site_footer = $this->entityTypeManager->getStorage('node')->create([
      'type' => 'uw_ct_site_footer',
      'uid' => 1,
      'title' => dt('Site footer'),
      'status' => 1,
      'field_site_footer_logo' => $logo,
      'field_site_footer_facebook' => rand(0, 1) ? NULL : 'university.waterloo',
      'field_site_footer_instagram' => rand(0, 1) ? NULL : 'uofwaterloo',
      'field_site_footer_linked_in' => rand(0, 1) ? NULL : 'uwaterloo',
      'field_site_footer_you_tube' => rand(0, 1) ? NULL : 'user/uwaterloo',
      'field_site_footer_twitter' => rand(0, 1) ? NULL : 'uWaterloo',
    ]);

    // Ensure that we only create 1 block for the site footer.
    $options['blocks'] = 1;

    // Array to store generated sections.
    $sections = $this->createSections($site_footer->uuid(), $options['blocks']);

    // Set the sections.
    $site_footer->layout_builder__layout->setValue($sections);
    $site_footer->set('moderation_state', 'published');

    // Save the node.
    $site_footer->save();

    // Clear the site footer cache.
    Cache::invalidateTags(['site_footer']);

    return $site_footer;
  }

  /**
   * {@inheritDoc}
   */
  public function generateService(array $options = [
    'media' => self::MEDIA_OPTIONAL_SHARED,
    'days-before' => -100,
    'days-after' => 100,
    'blocks' => 3,
  ]): EntityInterface {

    // Get some of the common fields.
    $common = $this->createCommonFields($options);

    // Get the service category from the taxonomy term.
    $service_categories = $this->entityTypeManager->getStorage('taxonomy_term')
      ->loadByProperties([
        'vid' => 'uw_vocab_service_categories',
      ]);
    $service_category = $service_categories[array_rand($service_categories)];

    // Create the new node with at least the required fields.
    // We are going to use a status of active, so that all
    // these services show on the site and on the listing
    // page.
    $service = $this->entityTypeManager->getStorage('node')->create([
      'type' => 'uw_ct_service',
      'uid' => 1,
      'title' => $this->textService->randomTitle(),
      'status' => 1,
      'field_uw_meta_description' => $this->textService->randomTitle(),
      'field_uw_service_summary' => [
        'value' => $this->textService->randomParagraphs(2),
        'format' => 'uw_tf_basic',
      ],
      'field_uw_service_status' => 'active',
      'field_uw_service_category' => $service_category,
    ]);

    // Array to store generated sections.
    $sections = $this->createSections($service->uuid(), $options['blocks']);

    // Set the sections.
    $service->layout_builder__layout->setValue($sections);
    $service->set('moderation_state', 'published');

    // Save the node.
    $service->save();

    return $service;
  }

  /**
   * {@inheritDoc}
   */
  public function createBlockBannerImages(): EntityInterface {
    $block = $this->blockContentStorage->create([
      'type' => 'uw_cbl_banner_images',
    ]);

    $banner_options = [
      'full-width',
      'inset',
      'split',
      'full-overlay',
    ];

    $block->field_uw_text_overlay_style->value = $banner_options[array_rand($banner_options)];
    $block->field_uw_autoplay->value = random_int(0, 1);
    $block->field_uw_slide_speed->value = random_int(5, 10) * 1000;
    $block->field_uw_transition_speed->value = random_int(4, 8) * 100;

    $all_images = $this->mediaService->allGeneratedMedia();

    $banner_image_ids = array_rand($all_images, random_int(2, 5));

    foreach ($banner_image_ids as $image_id) {
      if (in_array($image_id, $all_images)) {
        $paragraph = $this->createBlockBanner($image_id);
        $block->field_uw_banner_item[] = [
          'target_id' => $paragraph->id(),
          'target_revision_id' => $paragraph->getRevisionId(),
        ];
      }
    }

    // Set the title of the block.
    $block->setInfo($this->textService->randomTitle());
    $block->info->value = $this->textService->randomTitle();

    $block->save();

    return $block;
  }

  /**
   * Creates one banner (paragraph) entry.
   *
   * @param string $mid
   *   Media id.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   *   Exception with entity.
   */
  private function createBlockBanner(string $mid): EntityInterface {
    // Creating only image banners.
    $paragraph = Paragraph::create([
      'type' => 'uw_para_image_banner',
      'field_uw_ban_big_text' => ['value' => $this->textService->randomTitleShort()],
      'field_uw_ban_image' => ['target_id' => $mid],
      'field_uw_ban_link' => ['uri' => 'https://uwaterloo.ca'],
      'field_uw_ban_small_text' => ['value' => $this->textService->randomTitle()],
    ]);

    $paragraph->save();

    return $paragraph;
  }

  /**
   * {@inheritDoc}
   */
  public function createBlockCallToAction(): EntityInterface {
    $block = $this->blockContentStorage->create([
      'type' => 'uw_cbl_call_to_action',
    ]);

    for ($i = 0; $i < random_int(1, 5); $i++) {
      $paragraph = $this->createCallToActionParagraph();
      $block->field_uw_cta_details[] = [
        'target_id' => $paragraph->id(),
        'target_revision_id' => $paragraph->getRevisionId(),
      ];
    }

    // Set the title of the block.
    $block->setInfo($this->textService->randomTitle());
    $block->info->value = $this->textService->randomTitle();

    $block->save();

    // Return the newly created block as entity.
    return $block;
  }

  /**
   * Creates single CTA entity.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   Paragraph entity of cta type.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  private function createCallToActionParagraph(): EntityInterface {
    $styles = ['small', 'medium', 'big'];
    shuffle($styles);
    $style = reset($styles);

    $themes = [
      'org-default',
      'neutral',
      'org-art',
      'org-eng',
      'org-env',
      'org-ahs',
      'org-mat',
      'org-sci',
      'org-school',
    ];

    shuffle($themes);
    $theme = reset($themes);

    // Creating only image banners.
    $paragraph = Paragraph::create([
      'type' => 'uw_para_call_to_action',
      'field_uw_cta_link' => [
        'title' => $this->textService->randomTitleShort(),
        'uri' => 'https://google.ca',
      ],
      'field_uw_cta_text_details' => [
        'style' => $style,
        'text_value' => $this->textService->randomTitleShort(20),
      ],
      'field_uw_cta_theme' => ['value' => $theme],
    ]);

    $paragraph->save();

    return $paragraph;
  }

  /**
   * {@inheritDoc}
   */
  public function createBlockFactAndFigures(): EntityInterface {
    $block = $this->blockContentStorage->create([
      'type' => 'uw_cbl_facts_and_figures',
    ]);

    for ($i = 0; $i < random_int(2, 4); $i++) {
      $ff_group = $this->createFactsAndFiguresParagraph();

      $block->field_uw_ff_fact_figure[] = [
        'target_id' => $ff_group->id(),
        'target_revision_id' => $ff_group->getRevisionId(),
      ];
    }

    // Set the title of the block.
    $block->setInfo($this->textService->randomTitle());
    $block->info->value = $this->textService->randomTitle();

    $block->save();

    // Return the newly created block as entity.
    return $block;
  }

  /**
   * Creates facts and figures paragraph entity.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   Paragraph.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  private function createFactsAndFiguresParagraph(): EntityInterface {
    $colours = ['grey', 'black', 'white', 'gold'];
    shuffle($colours);
    $colour = reset($colours);

    $themes = [
      'org-default',
      'org-art',
      'org-eng',
      'org-env',
      'org-ahs',
      'org-mat',
      'org-sci',
    ];

    shuffle($themes);
    $theme = reset($themes);

    $alignments = ['left', 'right', 'center'];
    shuffle($alignments);
    $alignment = reset($alignments);

    $inner_paragraph = $this->createFactsAndFiguresInnerParagraph();

    // Creating only image banners.
    $paragraph = Paragraph::create([
      'type' => 'uw_para_ff',
      'field_uw_ff_dbg_color' => ['value' => $colour],
      'field_uw_ff_def_color' => ['value' => $theme],
      'field_uw_ff_show_bubbles' => ['value' => rand(0, 1) ? '1' : '0'],
      'field_uw_ff_textalign' => ['value' => $alignment],
      'field_uw_fact_figure' => [
        'target_id' => $inner_paragraph->id(),
        'target_revision_id' => $inner_paragraph->getRevisionId(),
      ],
    ]);

    $paragraph->save();

    return $paragraph;
  }

  /**
   * Generates inner facts and figures paragraph.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   Facts and figures paragraph entity.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  private function createFactsAndFiguresInnerParagraph(): EntityInterface {
    $styles = ['small', 'medium', 'big'];
    shuffle($styles);
    $style = reset($styles);

    $paragraph = Paragraph::create([
      'type' => 'uw_para_fact_figure',
      'field_uw_ff_info' => [
        'style' => $style,
        'text_value' => $this->textService->randomTitleShort(20),
      ],
    ]);

    $paragraph->save();

    return $paragraph;
  }

  /**
   * {@inheritDoc}
   */
  public function createBlockImageGallery(): EntityInterface {
    // TODO: Implement createBlockImageGallery() method.
  }

  /**
   * {@inheritDoc}
   */
  public function createBlockRelatedLinks(): EntityInterface {
    // TODO: Implement createBlockRelatedLinks() method.
  }

  /**
   * {@inheritDoc}
   */
  public function generateCatalogItem(array $options = [
    'media' => self::MEDIA_OPTIONAL_SHARED,
    'days-before' => -100,
    'days-after' => 100,
    'blocks' => 3,
  ],
  bool $published = TRUE
  ): EntityInterface {

    $catalogs = $this->entityTypeManager->getStorage('taxonomy_term')
      ->loadByProperties([
        'vid' => 'uw_vocab_catalogs',
      ]);

    $catalog = $catalogs[array_rand($catalogs)];

    // Generate common fields used across multiple content types.
    $common = $this->createCommonFields($options);

    $node = $this->entityTypeManager->getStorage('node')->create([
      'type' => 'uw_ct_catalog_item',
      'uid' => 1,
      'title' => $this->textService->randomTitle(),
      'status' => $published,
      'created' => $common['created'],
      'field_uw_catalog_summary' => [
        'value' => $this->textService->randomParagraphs(2),
        'format' => self::TEXT_FORMAT_BASIC,
      ],
      'field_uw_meta_description' => $this->textService->randomTitle(),
      'field_uw_catalog_catalog' => $catalog,
    ]);

    // Array to store generated sections.
    $sections = $this->createSections($node->uuid(), $options['blocks']);

    // Set the sections.
    $node->layout_builder__layout->setValue($sections);
    if ($published) {
      $node->set('moderation_state', 'published');
    }

    // Save the node.
    $node->save();

    return $node;
  }

  /**
   * {@inheritDoc}
   */
  public function generateOpportunity(array $options = [
    'media' => self::MEDIA_OPTIONAL_SHARED,
    'days-before' => 0,
    'days-after' => 100,
    'blocks' => 3,
  ], bool $published = TRUE): EntityInterface {

    // Generate common fields used across multiple content types.
    $common = $this->createCommonFields($options);

    // Get an opportunity type using the taxonomy term.
    $opportunity_types = $this->entityTypeManager->getStorage('taxonomy_term')
      ->loadByProperties([
        'vid' => 'uw_vocab_opportunity_type',
      ]);
    $opportunity_type = $opportunity_types[array_rand($opportunity_types)];

    // If this is a paid opportunity, need a rate of pay
    // as it is mandatory.
    if ($opportunity_type->getName() == 'Paid') {
      $rate_of_pay = '$' . rand(10, 50);
    }

    // Get an employmenet type using the taxonomy term.
    $employment_types = $this->entityTypeManager->getStorage('taxonomy_term')
      ->loadByProperties([
        'vid' => 'uw_vocab_opportunity_employment',
      ]);
    $employment_type = $employment_types[array_rand($employment_types)];

    // Create a new node with at least the required fields.
    $node = $this->entityTypeManager->getStorage('node')->create([
      'type' => 'uw_ct_opportunity',
      'uid' => 1,
      'title' => $this->textService->randomTitle(),
      'status' => $published,
      'created' => $common['created'],
      'field_uw_opportunity_position' => [
        'value' => $this->textService->randomParagraphs(2),
        'format' => self::TEXT_FORMAT_BASIC,
      ],
      'field_uw_opportunity_type' => $opportunity_type,
      'field_uw_opportunity_pos_number' => 1,
      'field_uw_opportunity_post_by' => $this->textService->randomTitle(),
      'field_uw_meta_description' => $this->textService->randomTitle(),
      'field_uw_opportunity_date' => $common['date'],
      'field_uw_opportunity_employment' => $employment_type,
      'field_uw_opportunity_pay_rate' => $rate_of_pay ?? NULL,
    ]);

    // Array to store generated sections.
    $sections = $this->createSections($node->uuid(), $options['blocks']);

    // Set the sections.
    $node->layout_builder__layout->setValue($sections);
    if ($published) {
      $node->set('moderation_state', 'published');
    }

    // Save the node.
    $node->save();

    return $node;
  }

  /**
   * Create code blocks.
   *
   * @param array $blocks
   *   Number of blocks to create.
   *
   * @return array
   *   Plugin/block configuration.
   */
  private function createRandomCodeBlocks(array $blocks): array {
    return [
      [
        'id' => 'uw_cbl_image',
        'provider' => 'uw_custom_blocks',
        'image' => $this->mediaService->randomMediaId(),
        'type_of_image' => 'full',
        'type_of_sized_image' => NULL,
        'scale_by' => NULL,
        'width' => NULL,
        'height' => NULL,
        'image_alignment' => NULL,
        'context_mapping' => NULL,
      ],
      [
        'id' => 'uw_cbl_image',
        'provider' => 'uw_custom_blocks',
        'image' => $this->mediaService->randomMediaId(),
        'type_of_image' => 'full',
        'type_of_sized_image' => NULL,
        'scale_by' => NULL,
        'width' => NULL,
        'height' => NULL,
        'image_alignment' => NULL,
        'context_mapping' => NULL,
      ],
    ];
  }

}
