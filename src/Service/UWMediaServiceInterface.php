<?php

namespace Drupal\uw_wcms_gen\Service;

use Drupal\Core\Entity\EntityInterface;

/**
 * Interface Media service.
 *
 * Holds example images list. And defines functions how to use this interface.
 *
 * @package Drupal\uw_wcms_gen\Service
 */
interface UWMediaServiceInterface {

  /**
   * Name of state variable.
   *
   * @var string
   */
  const STATE_ID = 'uw_wcms_gen.media';

  /**
   * Destination folder for upload.
   *
   * @var string
   */
  const DESTINATION_FOLDER = 'public://uploads/images/';

  /**
   * All demo images that can be used.
   *
   * @var string[]
   */
  const MEDIA_IMAGES = [
    '01_books.jpg' => [
      'credit' => 'Photo by Inaki del Olmo @ Unsplash',
      'alt' => 'Assorted title of books on shelves',
    ],
    '02_books2.jpg' => [
      'credit' => 'Photo by Kimberly Farmer @ Unsplash',
      'alt' => 'Shallow focus photography of books',
    ],
    '03_camping.jpg' => [
      'credit' => 'Photo by Dominik Jirovsky',
      'alt' => 'Person lying inside tent and overlooking mountains',
    ],
    '04_class.jpg' => [
      'credit' => 'Photo by Mikael Kristenson @ Unsplash',
      'alt' => 'Group of people sitting in chairs on conference',
    ],
    '05_desktop.jpg' => [
      'credit' => 'Photo by Marvin Meyer',
      'alt' => 'People sitting down near table with assorted laptop computers',
    ],
    '06_empty_class.jpg' => [
      'credit' => 'Photo by Nathan Dumlao',
      'alt' => 'Empty chairs in theatre',
    ],
    '07_graduation.jpg' => [
      'credit' => 'Photo by Vasily Koloda @ Unsplash',
      'alt' => 'Graduate students with hat in the air',
    ],
    '08_library.jpg' => [
      'credit' => 'Photo by Redd @ Unsplash',
      'alt' => 'Person with backpack besides books',
    ],
    '09_student.jpg' => [
      'credit' => 'Photo by Honey Yanibel Minaya Cruz @ Unsplash',
      'alt' => 'Person wearing red graduation dress',
    ],
    '10_studying.jpg' => [
      'credit' => 'Photo by Green Chameleon @ Unsplash',
      'alt' => 'Person writing on brown wooden table near white ceramic mug',
    ],
    '11_university.jpg' => [
      'credit' => 'Photo by Dom Fou @ Unsplash',
      'alt' => 'People sitting on chairs in front of computer photo',
    ],
    '12_another_books.jpg' => [
      'credit' => 'Photo by Sharon McCutcheon @ Unsplash',
      'alt' => 'Assorted books on wooden table',
    ],
    '13_white_desk.jpg' => [
      'credit' => 'Photo by Samantha Gades @ Unsplash',
      'alt' => 'White desk with lamp and plant on top',
    ],
    '14_stacked_books_portrait.jpg' => [
      'credit' => 'Photo by Leather Diary Studio @ Unsplash',
      'alt' => 'Stacked notebooks in various colours',
    ],
    '15_robotics.jpg' => [
      'credit' => 'Photo by ThisisEngineering RAEng @ Unsplash',
      'alt' => 'Robotics testing tennis balls',
    ],
    '16_electronics.jpg' => [
      'credit' => 'Photo by Jeswin Thomas @ Unsplash',
      'alt' => 'Person works on circuit board',
    ],
    '17_pencils.jpg' => [
      'credit' => 'Photo by Pierre Bamin @ Unsplash',
      'alt' => 'Various colours pencils in a jar',
    ],
    '18_exam.jpg' => [
      'credit' => 'Photo by Nick Morrison @ Unsplash',
      'alt' => 'Laptop and notepad on desk',
    ],
    '30_portrait_1.jpg' => ['credit' => 'Photo by Mathias Huysmans @ Unsplash', 'alt' => 'Woman in pink crew-neck shirt in closeup photography'],
    '31_portrait_2.jpg' => ['credit' => 'Photo by Michael Dam @ Unsplash', 'alt' => 'Closeup photography of woman smiling'],
    '32_portrait_3.jpg' => ['credit' => 'Photo by Jurica Koletic @ Unsplash', 'alt' => 'Man wearing Henley top portrait'],
    '33_portrait_4.jpg' => ['credit' => 'Photo from Unspalsh', 'alt' => 'Man in plaid shirt portrait.'],
    '50_pen.png' => ['credit' => '', 'alt' => 'Ballpoint pen'],
    '51_plant_1.png' => ['credit' => '', 'alt' => 'Desk plant from above'],
    '52_plant_2.png' => ['credit' => '', 'alt' => 'Desk plant from above'],
  ];

  /**
   * Create media entity for provided image name.
   */
  public function createMedia(string $name = '');

  /**
   * Creates file/media entities for all demo images.
   *
   * Sets state that will keep track of these demo images, so it can be removed.
   *
   * @return bool
   *   TRUE if import was successful. FALSE otherwise, including second import.
   */
  public function createAllMedia(): bool;

  /**
   * From previously created media entities, randomly picks one and returns.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   Loaded media object.
   */
  public function randomMedia(): ?EntityInterface;

  /**
   * Picks demo image media id.
   *
   * @return int
   *   Returns media id.
   */
  public function randomMediaId(): int;

  /**
   * Removes all demo images/media based on state.
   *
   * @return bool
   *   TRUE if removal was successful. FALSE otherwise.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function cleanUp(): bool;

  /**
   * Return all generated media stored in state.
   *
   * @return array
   *   If exists return array of media ids.
   */
  public function allGeneratedMedia(): array;

}
