<?php

namespace Drupal\uw_wcms_gen\Service;

/**
 * Custom text service interface.
 */
interface UWTextServiceInterface {

  /**
   * Name of state variable that holds all tags.
   *
   * @var string
   */
  const STATE_ID = 'uw_wcms_gen.tags';

  /**
   * Get all titles in one request.
   *
   * @param int $items
   *   Now many paragraphs to pull, number of title sentences will be greater.
   *
   * @return array
   *   List of items.
   */
  public function getTitles(int $items = 10): array;

  /**
   * Get random paragraphs of text.
   *
   * @param int $items
   *   How many paragraphs to return.
   *
   * @return array
   *   Paragraphs on text.
   */
  public function getParagraphs(int $items = 20): array;

  /**
   * Function that creates random data in range.
   *
   * @param int $before
   *   How many days to ge back. Defaults to 100 days.
   * @param int $after
   *   How many days in the future. For future content. Defaults to 100.
   * @param string $format
   *   Default format.
   *
   * @return string
   *   Random date formatted.
   */
  public function randomDate(int $before = -100, int $after = 100, string $format = 'Y-m-d'): string;

  /**
   * Creates random timestamp from provided range in days.
   *
   * To create only timestamps in the past, provide after as zero. Same goes
   * for events in the future, provide before as zero (today).
   *
   * @param int $before
   *   Number of days (today - before) as start of range.
   * @param int $after
   *   Number of days (today + after) as end of range.
   * @param bool $round_to_day
   *   If result needs to be rounded to a day.
   * @param string $timezone
   *   Timezone.
   *
   * @return int
   *   Random unix timestamp with in the range.
   */
  public function randomTimestamp(int $before = -100, int $after = 100, bool $round_to_day = TRUE, string $timezone = 'America/Toronto'): int;

  /**
   * Returns random title from array of titles.
   *
   * @return string
   *   Random title from api results.
   */
  public function randomTitle(): string;

  /**
   * Creates paragraphs of text.
   *
   * @param int $paragraphs
   *   Number of paragraphs to return.
   *
   * @return string
   *   HTML tags (p) wrapped paragraphs.
   */
  public function randomParagraphs(int $paragraphs): string;

  /**
   * Get list of random names.
   *
   * @param int $items
   *   How many names to fetch.
   *
   * @return array
   *   List of random names.
   */
  public function getNames(int $items = 20): array;

  /**
   * Returns random name.
   *
   * @return string
   *   Random name from pre-generated list.
   */
  public function randomName(): string;

  /**
   * Returns random position.
   *
   * @return string
   *   Random position.
   */
  public function randomPosition(): string;

  /**
   * Create tags.
   */
  public function createTags(): void;

  /**
   * Returns random tag.
   *
   * @param string $vocab
   *   Which vocabulary to use, default to 'blog', other options (news, events).
   * @param int $number
   *   Number of tags to return. Defaults to 1.
   *
   * @return array
   *   List of tags.
   */
  public function randomTag(string $vocab = 'blog', int $number = 1): array;

  /**
   * Return short title.
   *
   * @param int $width
   *   How long title should be, in characters.
   *
   * @return string
   *   Random title.
   */
  public function randomTitleShort(int $width = 128): string;

}
