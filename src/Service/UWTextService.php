<?php

namespace Drupal\uw_wcms_gen\Service;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\State\StateInterface;
use GuzzleHttp\ClientInterface;

/**
 * Random text and date service.
 *
 * @package Drupal\uw_wcms_gen\Service
 */
class UWTextService implements UWTextServiceInterface {

  /**
   * Seconds per day, this number will be multiplied with number of days.
   */
  const SECONDS_IN_DAY = 84600;

  /**
   * How many items to request from API (titles and paragraphs).
   */
  const FETCH_SIZE = 20;

  /**
   * If count for a random values of array falls below this limit, append more.
   */
  const SIZE_BEFORE_RECREATE = 5;

  /**
   * API endpoints for title and paragraph text.
   */
  const TITLE_API_ENDPOINT = 'http://metaphorpsum.com/sentences/';
  const PARAGRAPH_API_ENDPOINT = 'http://metaphorpsum.com/paragraphs/';

  /**
   * Random first name pool.
   */
  const FIRST_NAMES = [
    'Jordan', 'Jett', 'Sean', 'Lucas', 'Derek', 'Felix', 'Isaac', 'Joel', 'Nicholas', 'Ezra', 'Caleb', 'Maxwell', 'Mason', 'Hugo', 'Jack',
    'Amy', 'Isabella', 'Emily', 'Macy', 'Caroline', 'Adrianna', 'Claire', 'Eleanor', 'Hope', 'Mia', 'Kiara', 'Addison', 'Maya', 'Eve', 'Alexa',
    'Mathis', 'Jerome', 'Emus', 'Sebastien', 'Camille', 'Fleuvette', 'Ines', 'Bianca', 'Julianne', 'Adele',
  ];

  /**
   * Random last name pool.
   */
  const LAST_NAMES = [
    'Snow', 'Bishop', 'Harris', 'Ross', 'Wheeler', 'Murray', 'Woodward', 'Gill', 'Moore', 'Rogers',
    'Stewart', 'Clark', 'Dawson', 'Bell', 'Lee', 'Brown', 'McCain', 'Irving', 'Jackson', 'Meyer',
    'Laroche', 'Martinez', 'Comtois', 'Savard', 'Couturier', 'Millette', 'Labelle', 'Leboeuf', 'Chicoine', 'Latreille',
  ];

  /**
   * Random positions used for profile/contact.
   */
  const POSITIONS = [
    'Professor', 'Teacher assistant', 'Lecturer', 'Assistant professor',
    'Associate professor', 'Dean', 'Director', 'President', 'Librarian',
    'Strategic Initiatives Officer', 'Chief Financial Officer',
    'Vise-President', 'Career Services Specialist', 'Liaison Officer',
  ];

  /**
   * Random blog tags.
   */
  const BLOG_TAGS = [
    'University', 'Students', 'Alumni', 'Green energy', 'Sustainability',
    'Health', 'Celebration', 'Zero waste', 'Recycle', 'Conference',
    'Undergrad studies', 'Grad studies', 'Plastic', 'Greenhouse gas',
    'Fossil fuels', 'Renewable energy', 'Thermal energy', 'Electricity',
    'Hydro', 'Biodiversity', 'Strategy', 'Ecology', 'Conservation policy',
    'Summer term', 'Winter term', 'Spring term', 'Fall term',
  ];

  /**
   * Entity type manager from Drupal core.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Http client from Drupal core.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * State service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $stateService;

  /**
   * Random titles.
   *
   * @var array
   */
  protected $titles = NULL;

  /**
   * Random paragraphs.
   *
   * @var array
   */
  protected $paragraphs = NULL;

  /**
   * Random names.
   *
   * @var array
   */
  protected $names = NULL;

  /**
   * Constructs new service object.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, ClientInterface $httpClient, StateInterface $stateService) {
    $this->entityTypeManager = $entityTypeManager;
    $this->httpClient = $httpClient;
    $this->stateService = $stateService;
    // Initialize data with first batch of random values.
    $this->titles = $this->getTitles();
    $this->paragraphs = $this->getParagraphs();
    $this->names = $this->getNames();
  }

  /**
   * {@inheritDoc}
   */
  public function getTitles(int $items = self::FETCH_SIZE): array {
    $result = [];

    $url = self::TITLE_API_ENDPOINT . $items;

    $request = $this->httpClient->request('GET', $url);

    if ($request->getStatusCode() === 200) {
      $body = $request->getBody()->getContents();
      $result = array_filter(preg_split("/[.?!]+\s/", $body));
      array_walk($result, [$this, 'clearTitle']);
    }

    return $result;
  }

  /**
   * Trims sentence ending.
   *
   * @param string $item
   *   Input sentence that is modified.
   * @param int $key
   *   Array key of the sentence.
   */
  protected function clearTitle(string &$item, int $key): void {
    $item = rtrim($item, '.!?');
  }

  /**
   * {@inheritDoc}
   */
  public function getParagraphs(int $items = self::FETCH_SIZE): array {
    $result = [];
    $url = self::PARAGRAPH_API_ENDPOINT . $items;

    $request = $this->httpClient->request('GET', $url);
    if ($request->getStatusCode() === 200) {
      $body = $request->getBody()->getContents();
      $result = array_values(array_filter(preg_split("/[\r\n]/", $body)));
    }

    return $result;
  }

  /**
   * {@inheritDoc}
   */
  public function randomDate(int $before = -100, int $after = 100, string $format = 'Y-m-d'): string {
    return date($format, $this->randomTimestamp($before, $after));
  }

  /**
   * {@inheritDoc}
   */
  public function randomTimestamp(int $before = -100, int $after = 100, bool $round_to_day = TRUE, string $timezone = 'America/Toronto'): int {
    $lower_limit = self::SECONDS_IN_DAY * $before;
    $upper_limit = self::SECONDS_IN_DAY * $after;

    $time_diff = mt_rand($lower_limit, $upper_limit);
    $timestamp = time() + $time_diff;

    if ($round_to_day) {
      $date = DrupalDateTime::createFromTimestamp($timestamp, $timezone);
      $date->setTime(0, 0);

      $timestamp = $date->getTimestamp();
    }

    return $timestamp;
  }

  /**
   * Return random title.
   */
  public function randomTitle(): string {
    // If running low on random titles, add more generated ones.
    if (count($this->titles) < self::SIZE_BEFORE_RECREATE) {
      $this->titles += $this->getTitles();
    }

    $random_key = array_rand($this->titles);
    $title = $this->titles[$random_key];

    // Remove used title from array of random values.
    unset($this->titles[$random_key]);

    return $title;
  }

  /**
   * Generate paragraphs.
   *
   * @param int $paragraphs
   *   Number of paragraphs to generate.
   *
   * @return string
   *   Paragraphs wrapped in p html tag and merged to a string.
   */
  public function randomParagraphs(int $paragraphs = 1): string {
    $result = '';
    $text = [];

    // Need to iterate one by one, since logic will remove random item.
    for ($i = 0; $i < $paragraphs; $i++) {

      // Check if there are less than 5 paragraphs, if so, generate more.
      if (count($this->paragraphs) < self::SIZE_BEFORE_RECREATE) {
        $this->paragraphs += $this->getParagraphs();
      }

      $key = array_rand($this->paragraphs);

      $text[] = $this->paragraphs[$key];

      // Remove used paragraph from array of random available ones.
      unset($this->paragraphs[$key]);
    }

    if ($text) {
      // Wrap random paragraph in html p tag.
      array_walk($text, function (&$item, $key) {
        $item = '<p>' . $item . '</p>';
      });

      $result = implode(PHP_EOL, $text);
    }

    return $result;
  }

  /**
   * {@inheritDoc}
   */
  public function getNames(int $items = 30): array {
    $names = [];

    for ($x = 0; $x < $items; $x++) {
      $fn_index = array_rand(self::FIRST_NAMES);
      $ln_index = array_rand(self::LAST_NAMES);

      $names[] = self::FIRST_NAMES[$fn_index] . ' ' . self::LAST_NAMES[$ln_index];
    }

    return $names;
  }

  /**
   * {@inheritDoc}
   */
  public function randomName(): string {

    // Check if there are names to pick from, if count is less than 5, append
    // more names to list/array of available random names.
    if (count($this->names) < self::SIZE_BEFORE_RECREATE) {
      $this->names += $this->getNames();
    }

    $key = array_rand($this->names);
    $name = $this->names[$key];
    unset($this->names[$key]);

    return $name;
  }

  /**
   * {@inheritDoc}
   */
  public function randomPosition(): string {
    return self::POSITIONS[array_rand(self::POSITIONS)];
  }

  /**
   * {@inheritDoc}
   */
  public function createTags(): void {
    // Needs to check against existing terms, in case this command has been
    // run more than once. Ideally only one term should be created.
    $existing_terms = $this->stateService->get(self::STATE_ID, [
      'blog' => [],
      'events' => [],
      'news' => [],
    ]);

    // List of vocabularies to create tags in.
    $taxonomies = [
      'uw_vocab_blog_tags' => 'blog',
      'uw_tax_event_tags' => 'events',
      'uw_vocab_news_tags' => 'news',
    ];

    // Get storage once outside of the loops.
    $term_storage = $this->entityTypeManager->getStorage('taxonomy_term');

    // Loop over new tags to be created.
    foreach (self::BLOG_TAGS as $new_tag) {

      // Create same tag in three vocabularies, and keep track on ids for each.
      foreach ($taxonomies as $vocab => $name) {

        // If we have term already, just move to the next term.
        if (!empty($existing_terms[$name][$new_tag])) {
          continue;
        }

        // Try to load term by name. And try to find it in existing terms array.
        $existing_term = $term_storage->loadByProperties([
          'vid' => $vocab,
          'name' => $new_tag,
        ]);

        // There is term that matches properties. This term can be added by
        // user manually. All we need to do is keep track of term id.
        // Expecting only one result returned.
        if (!empty($existing_term)) {
          $existing_terms[$name][$new_tag] = reset($existing_term)->id();
        }
        else {
          $create_new_term = $term_storage->create([
            'vid' => $vocab,
            'name' => $new_tag,
          ]);
          $create_new_term->save();
          $existing_terms[$name][$new_tag] = $create_new_term->id();
        }

      }
    }

    // Save all terms.
    $this->stateService->set(self::STATE_ID, $existing_terms);
  }

  /**
   * {@inheritDoc}
   */
  public function randomTag(string $vocab = 'blog', int $number = 1): array {
    $tags = [];

    $exiting_tags = $this->stateService->get(self::STATE_ID);

    if (!empty($exiting_tags[$vocab])) {
      $all_tags = $exiting_tags[$vocab];
      if ($number === 1) {
        $tags[] = array_rand(array_flip($all_tags));
      }
      else {
        $tags = array_rand(array_flip($all_tags), $number);
      }
    }

    return $tags;
  }

  /**
   * {@inheritDoc}
   */
  public function randomTitleShort(int $width = 128): string {
    $title = $this->randomTitle();

    return strtok(wordwrap($title, $width, "\n\t", FALSE), "\n\t");
  }

}
