<?php

namespace Drupal\uw_wcms_gen\Service;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\State\StateInterface;
use Drupal\file\Entity\File;
use Drupal\media\Entity\Media;

/**
 * UW Media service.
 *
 * @package Drupal\uw_wcms_gen\Service
 */
class UWMediaService implements UWMediaServiceInterface {

  /**
   * Image source path.
   *
   * @var string
   */
  protected $imagePath;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * File system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * State service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $stateService;

  /**
   * Constructs new service object.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, FileSystemInterface $fileSystem, StateInterface $stateService) {
    $this->entityTypeManager = $entityTypeManager;
    $this->fileSystem = $fileSystem;
    $this->stateService = $stateService;

    // Set image path for easier access.
    $this->imagePath = drupal_get_path('module', 'uw_wcms_gen') . '/assets/images/';
  }

  /**
   * {@inheritDoc}
   */
  public function createMedia(string $name = '') {
    $source = $this->imagePath . $name;
    $destination = self::DESTINATION_FOLDER . $name;
    $destination_folder = self::DESTINATION_FOLDER;

    if (file_exists($source) && $this->fileSystem->prepareDirectory($destination_folder, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS)) {
      $this->fileSystem->copy($source, $destination);
      $image = File::create();
      $image->setFileUri($destination);
      $image->setOwnerId(1);
      $image->setMimeType('image/' . pathinfo($destination, PATHINFO_EXTENSION));
      $image->setFilename($name);
      $image->setPermanent();
      $image->save();

      $media = Media::create([
        'bundle' => 'uw_mt_image',
        'name' => $name,
        'uid' => 1,
        'field_media_image' => [
          'target_id' => $image->id(),
          'alt' => UWMediaServiceInterface::MEDIA_IMAGES[$name]['alt'],
        ],
        'field_uw_image_caption' => UWMediaServiceInterface::MEDIA_IMAGES[$name]['credit'],
      ]);

      $media->setPublished()
        ->save();

      // Update state by adding new media id.
      $medias = $this->stateService->get(self::STATE_ID, []);
      $medias[] = $media->id();
      $this->stateService->set(self::STATE_ID, $medias);
    }
  }

  /**
   * {@inheritDoc}
   */
  public function createAllMedia(): bool {

    // Check if this function was run previously.
    if (empty($this->stateService->get(self::STATE_ID, []))) {
      foreach (self::MEDIA_IMAGES as $name => $details) {
        $this->createMedia($name);
      }
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  /**
   * {@inheritDoc}
   */
  public function randomMedia(): ?EntityInterface {
    $media = NULL;

    if ($mid = $this->randomMediaId() !== -1) {
      $media = $this->entityTypeManager->getStorage('media')->load($mid);
    }

    return $media;
  }

  /**
   * {@inheritDoc}
   */
  public function randomMediaId(): int {
    $random = -1;
    $medias = $this->stateService->get(self::STATE_ID, []);

    if (!empty($medias)) {
      $random = array_rand($medias);
    }

    return $random;
  }

  /**
   * {@inheritDoc}
   */
  public function cleanUp(): bool {
    $result = FALSE;
    $medias = $this->stateService->get(self::STATE_ID, []);
    $media_storage = $this->entityTypeManager->getStorage('media');
    $entities = $media_storage->loadMultiple($medias);

    if ($entities) {
      foreach ($entities as $media_entity) {
        /** @var \Drupal\file\FileInterface $file_entity */
        $file_entity = $media_entity->field_media_image->entity;
        $media_entity->delete();

        if ($file_entity) {
          $file_entity->delete();
        }
      }

      $result = TRUE;
      // Set state to empty array.
      $this->stateService->set(self::STATE_ID, []);
    }

    return $result;
  }

  /**
   * {@inheritDoc}
   */
  public function allGeneratedMedia(): array {
    return $this->stateService->get(self::STATE_ID, []);
  }

}
