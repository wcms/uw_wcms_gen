<?php

namespace Drupal\uw_wcms_gen\Commands;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\taxonomy\Entity\Term;
use Drupal\user\Entity\User;
use Drupal\uw_wcms_gen\Service\UWMediaServiceInterface;
use Drupal\uw_wcms_gen\Service\UWNodeContentServiceInterface;
use Drush\Commands\DrushCommands;

/**
 * Drush commands for uw_wcms_gen module.
 *
 * @package Drupal\uw_wcms_gen\Commands
 */
class UwDrushCommands extends DrushCommands {

  /**
   * Maximum number of entities to generate. Not to abuse this.
   *
   * @var int
   */
  public const MAX_ENTITIES = 20;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * UW Content service that generates entities.
   *
   * @var \Drupal\uw_wcms_gen\Service\UWNodeContentServiceInterface
   */
  protected UWNodeContentServiceInterface $generator;

  /**
   * UW Media service that imports images, and creates files/media entities.
   *
   * @var \Drupal\uw_wcms_gen\Service\UWMediaServiceInterface
   */
  protected UWMediaServiceInterface $mediaApi;

  /**
   * Default construction for custom drush commands of uw_wcms_gen.
   */
  public function __construct(
    EntityTypeManagerInterface $entityTypeManager,
    UWNodeContentServiceInterface $contentService,
    UWMediaServiceInterface $mediaApi
  ) {

    parent::__construct();
    $this->entityTypeManager = $entityTypeManager;
    $this->generator = $contentService;
    $this->mediaApi = $mediaApi;
  }

  /**
   * Drush command to generate service categories.
   *
   * @param int $service_categories
   *   How many service categories to generate.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Exception
   *
   * @command uwgen:service-categories
   * @aliases uwgsc
   * @usage uwgen:service-categories 3 or uwgsc 3
   */
  public function generateServiceCategories(int $service_categories = 5): void {

    // Names of service categories to create.
    $data = [
      'Business',
      'Communication',
      'Construction',
      'Engineering',
      'Distribution',
      'Education',
      'Finance',
      'Health',
      'Recreation',
    ];

    // Randomize order.
    shuffle($data);

    // Get specified number of service categories to create.
    $data = array_slice($data, 0, $service_categories);

    // Load existing vocabulary by machine name.
    $service_vocabulary = $this->entityTypeManager->getStorage('taxonomy_vocabulary')
      ->load('uw_vocab_service_categories');

    if ($service_vocabulary && $serv_id = $service_vocabulary->id()) {

      foreach ($data as $name) {

        Term::create([
          'name' => $name,
          'vid' => $serv_id,
        ])->save();
      }
    }

    $this->logger()->success(dt('Successfully generated @num service categories.', ['@num' => $service_categories]));
  }

  /**
   * Drush command to generate Catalogs.
   *
   * @param int $catalogs
   *   How many catalogs to generate.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Exception
   *
   * @command uwgen:catalog
   * @aliases uwgtc
   * @usage uwgen:catalog 3 or uwgtc 3
   */
  public function generateCatalogs(int $catalogs = 5): void {

    // Names of catalogs to create.
    $data = [
      'Solar system',
      'Animals',
      'Colours',
      'Movies',
      'Artists',
      'Painters',
      'Festivals',
      'Books',
      'Social networks',
    ];

    // Randomize order.
    shuffle($data);

    // Get specified number of catalogs to create.
    $data = array_slice($data, 0, $catalogs);

    // Load existing vocabulary by machine name.
    $catalog_vocabulary = $this->entityTypeManager->getStorage('taxonomy_vocabulary')
      ->load('uw_vocab_catalogs');

    if ($catalog_vocabulary && $cat_id = $catalog_vocabulary->id()) {
      // Tab name values available.
      $tab_names = [
        'New',
        'Popular',
        'Category',
        'Audience',
        'Faculty',
      ];

      foreach ($data as $name) {
        $tabs = [];

        // Use random to select tabs to display.
        foreach ($tab_names as $tab_name) {
          if (random_int(0, 1)) {
            $tabs[] = ['value' => $tab_name];
          }
        }

        Term::create([
          'name' => $name,
          'vid' => $cat_id,
          'field_uw_catalog_tabs_display' => $tabs,
        ])->save();
      }
    }

    $this->logger()->success(dt('Successfully generated @num catalog(s).', ['@num' => $catalogs]));
  }

  /**
   * Drush command to generate users.
   *
   * @param string $prole
   *   Use specific role, only if found.
   *
   * @command uwgen:users
   * @aliases uwgu
   * @usage uwgen:users, uwgu uw_role_site_owner
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function generateUsers(string $prole = ''): void {
    $users = [
      'uw_role_site_owner' => 'Site Owner',
      'uw_role_site_manager' => 'Site Manager',
      'uw_role_content_editor' => 'Content Editor',
      'uw_role_content_author' => 'Content Author',
      'uw_role_form_editor' => 'Form Editor',
      'uw_role_form_results_access' => 'Form Results',
      'authenticated' => 'Auth User',
    ];

    if ($prole) {
      if (isset($users[$prole])) {
        $users = [$prole => $users[$prole]];
      }
      else {
        $this->logger()->warning('Role not found, skipping creation.');
        exit();
      }
    }

    foreach ($users as $role => $name) {
      $parts = explode(' ', $name);

      $username = strtolower(implode('', $parts));
      $email = $username . '@example.com';

      if (!user_load_by_name($username)) {
        $user = User::create([
          'mail' => $email,
          'username' => $username,
          'pass' => $username,
          'name' => $username,
        ]);

        // Skip role assign for just authenticated role.
        if ($role !== 'authenticated') {
          $user->addRole($role);
        }
        $user->activate();

        $user->save();
        $this->logger()->notice(dt('Generated user @name with role @role.',
            ['@name' => $name, '@role' => $role])
        );
      }
      else {
        $this->logger()->warning(dt('User @username exists, skipping creation.', ['@username' => $username]));
      }
    }
  }

  /**
   * Creates demo media entities, keep tracks of generated ones.
   *
   * @command uwgen:media
   * @aliases uwgm
   * @usage uwgen:media, uwgm
   */
  public function generateMedia(): void {
    $this->mediaApi->createAllMedia();
    $generated = $this->mediaApi->allGeneratedMedia();

    $this->logger()->success(dt('Generate @num media entities.', ['@num' => count($generated)]));
  }

  /**
   * Runs clean up, deletes all media/files entities.
   *
   * @command uwgen:cleanup
   * @aliases uwgcc
   * @usage uwgen:cleanup, uwgcc
   */
  public function cleanUpDemoMedia(): void {
    $this->mediaApi->cleanUp();

    $this->logger()->success('All demo media and files entities deleted.');
  }

  /**
   * Drush command to generate blogs.
   *
   * @param int $blogs
   *   How many nodes to generate.
   * @param bool $status
   *   Default state of newly created content. Defaults to published.
   * @param array $options
   *   Additional options passed to command.
   *
   * @command uwgen:blog
   * @aliases uwgb
   * @usage uwgen:blog, uwgen:blog 5, uwgenb 5
   * @option blogs
   *   How many blogs to generate. Max 20.
   * @option media
   *   Option for media. Option "1" media is required and shared (same media
   *   for listing page and hero). Option "2" is media is optional (random) but
   *   shared with listing and hero. Option "3" is optional media but
   *   shared. While option "4" is independent media (each field for itself)
   *   and optional.
   * @option days-before
   *   Int number of days to use for random function as left limit (from).
   * @option days-after
   *   Int number of days to use for random function as right limit (to).
   * @option blocks
   *   How many layout builder blocks per node.
   */
  public function generateContentBlog(int $blogs = 1, bool $status = TRUE, array $options = [
    'media' => UWNodeContentServiceInterface::MEDIA_OPTIONAL_SHARED,
    'days-before' => -100,
    'days-after' => 100,
    'blocks' => 3,
  ]): void {

    // Check if media has been created prior.
    if (empty($this->mediaApi->allGeneratedMedia())) {
      $this->logger()->warning('No media detected. Creating media entities now.');
      $this->mediaApi->createAllMedia();
    }

    if ($blogs > 0 && $blogs <= self::MAX_ENTITIES) {
      for ($i = 0; $i < $blogs; $i++) {
        $this->generator->generateBlog($options, $status);
      }

      $this->logger()->success(dt('Generated @num blog(s).', ['@num' => $blogs]));
    }
    else {
      $this->logger()->error(dt('Please provide positive int argument not greater than @max for numbers of blog post to generate.', ['@max' => self::MAX_ENTITIES]));
    }
  }

  /**
   * Drush command to generate news items.
   *
   * @param int $number
   *   How many nodes to generate.
   * @param array $options
   *   Additional options passed to command.
   *
   * @command uwgen:news
   * @aliases uwgn
   * @usage uwgen:news, uwgen:news 5, uwgn 5
   * @option number
   *   How many items to generate. Max 20.
   * @option media
   *   Option for media. Option "1" media is required and shared (same media
   *   for listing page and hero). Option "2" is media is optional (random) but
   *   shared with listing and hero. Option "3" is optional media but
   *   shared. While option "4" is independent media (each field for itself)
   *   and optional.
   * @option days-before
   *   Int number of days to use for random function as left limit (from).
   * @option days-after
   *   Int number of days to use for random function as right limit (to).
   * @option blocks
   *   How many layout builder blocks per node.
   */
  public function generateContentNewsItem(int $number = 1, array $options = [
    'media' => UWNodeContentServiceInterface::MEDIA_OPTIONAL_SHARED,
    'days-before' => -100,
    'days-after' => 100,
    'blocks' => 3,
  ]): void {

    // Check if media has been created prior.
    if (empty($this->mediaApi->allGeneratedMedia())) {
      $this->logger()->warning('No media detected. Creating media entities now.');
      $this->mediaApi->createAllMedia();
    }

    if ($number > 0 && $number <= self::MAX_ENTITIES) {
      for ($i = 0; $i < $number; $i++) {
        $this->generator->generateNewsItem($options);
      }

      $this->logger()->success(dt('Generated @num news item(s).', ['@num' => $number]));
    }
    else {
      $this->logger()->error(dt('Please provide positive int argument not greater than @max for numbers of blog post to generate.', ['@max' => self::MAX_ENTITIES]));
    }
  }

  /**
   * Drush command to generate events.
   *
   * @param int $number
   *   How many nodes to generate.
   * @param array $options
   *   Additional options passed to command.
   *
   * @command uwgen:event
   * @aliases uwge
   * @usage uwgen:event, uwgen:event 5, uwge 5
   * @option number
   *   How many items to generate. Max 20.
   * @option media
   *   Option for media. Option "1" media is required and shared (same media
   *   for listing page and hero). Option "2" is media is optional (random) but
   *   shared with listing and hero. Option "3" is optional media but
   *   shared. While option "4" is independent media (each field for itself)
   *   and optional.
   * @option days-before
   *   Int number of days to use for random function as left limit (from).
   * @option days-after
   *   Int number of days to use for random function as right limit (to).
   * @option blocks
   *   How many layout builder blocks per node.
   */
  public function generateContentEvent(int $number = 1, array $options = [
    'media' => UWNodeContentServiceInterface::MEDIA_OPTIONAL_SHARED,
    'days-before' => -100,
    'days-after' => 100,
    'blocks' => 3,
  ]): void {

    // Check if media has been created prior.
    if (empty($this->mediaApi->allGeneratedMedia())) {
      $this->logger()->warning('No media detected. Creating media entities now.');
      $this->mediaApi->createAllMedia();
    }

    if ($number > 0 && $number <= self::MAX_ENTITIES) {
      for ($i = 0; $i < $number; $i++) {
        $this->generator->generateEvent($options);
      }

      $this->logger()->success(dt('Generated @num event(s).', ['@num' => $number]));
    }
    else {
      $this->logger()->error(dt('Please provide positive int argument not greater than @max for numbers of blog post to generate.', ['@max' => self::MAX_ENTITIES]));
    }
  }

  /**
   * Drush command to generate web page.
   *
   * @param int $number
   *   How many nodes to generate.
   * @param array $options
   *   Additional options passed to command.
   *
   * @command uwgen:webpage
   * @aliases uwgw
   * @usage uwgen:webpage, uwgen:webpage 5, uwgw 5
   * @option number
   *   How many items to generate. Max 20.
   * @option media
   *   Option for media. Option "1" media is required and shared (same media
   *   for listing page and hero). Option "2" is media is optional (random) but
   *   shared with listing and hero. Option "3" is optional media but
   *   shared. While option "4" is independent media (each field for itself)
   *   and optional.
   * @option days-before
   *   Int number of days to use for random function as left limit (from).
   * @option days-after
   *   Int number of days to use for random function as right limit (to).
   * @option blocks
   *   How many layout builder blocks per node.
   */
  public function generateContentWebpage(int $number = 1, array $options = [
    'media' => UWNodeContentServiceInterface::MEDIA_OPTIONAL_SHARED,
    'days-before' => -100,
    'days-after' => 100,
    'blocks' => 3,
  ]): void {

    // Check if media has been created prior.
    if (empty($this->mediaApi->allGeneratedMedia())) {
      $this->logger()->warning('No media detected. Creating media entities now.');
      $this->mediaApi->createAllMedia();
    }

    if ($number > 0 && $number <= self::MAX_ENTITIES) {
      for ($i = 0; $i < $number; $i++) {
        $this->generator->generateWebpage($options);
      }

      $this->logger()->success(dt('Generated @num web page(s).', ['@num' => $number]));
    }
    else {
      $this->logger()->error(dt('Please provide positive int argument not greater than @max for numbers of blog post to generate.', ['@max' => self::MAX_ENTITIES]));
    }
  }

  /**
   * Drush command to generate project.
   *
   * @param int $items
   *   Number of profile to generate.
   * @param array $options
   *   Additional options.
   *
   * @command uwgen:project
   * @aliases uwgpj
   * @usage uwgen:project, uwgen:project 5, uwgpj 5
   * @option media
   *   To include media or not.
   * @option options
   *   Number of blocks on layout builder page.
   */
  public function generateContentProject(int $items = 1, array $options = [
    'media' => UWNodeContentServiceInterface::MEDIA_REQUIRED_SHARED,
    'days-before' => -100,
    'days-after' => 100,
    'blocks' => 3,
  ]): void {

    // Check if media has been created prior.
    if (empty($this->mediaApi->allGeneratedMedia())) {
      $this->logger()->warning('No media detected. Creating media entities now.');
      $this->mediaApi->createAllMedia();
    }

    if ($items > 0 && $items <= self::MAX_ENTITIES) {
      for ($i = 0; $i < $items; $i++) {
        $this->generator->generateProject($options);
      }

      $this->logger()->success(dt('Generated @num project(s).', ['@num' => $items]));
    }
    else {
      $this->logger()->error(dt('Please provide positive int argument not greater than @max for numbers of blog post to generate.', ['@max' => self::MAX_ENTITIES]));
    }
  }

  /**
   * Drush command to generate site footer.
   *
   * @param int $items
   *   Number of profile to generate.
   * @param array $options
   *   Additional options.
   *
   * @command uwgen:site-footer
   * @aliases uwgsf
   * @usage uwgen:site-footer, uwgsf
   * @option media
   *   To include media or not.
   * @option options
   *   Number of blocks on layout builder page.
   */
  public function generateContentSiteFooter(int $items = 1, array $options = [
    'media' => UWNodeContentServiceInterface::MEDIA_REQUIRED_SHARED,
    'days-before' => -100,
    'days-after' => 100,
    'blocks' => 1,
  ]): void {

    // If there is a site footer, set an error and return.
    if ($this->generator->checkSiteFooter()) {
      $this->logger()->error(dt('There is already a site footer.'));
      return;
    }

    // Check if media has been created prior.
    if (empty($this->mediaApi->allGeneratedMedia())) {
      $this->logger()->warning('No media detected. Creating media entities now.');
      $this->mediaApi->createAllMedia();
    }

    // We can only ever have one site footer, so
    // set the number of items to 1 no matter what.
    $this->generator->generateSiteFooter($options);
    $this->logger()->success(dt('Generated site footer.'));
  }

  /**
   * Drush command to generate contact.
   *
   * @param int $items
   *   Number of contact to generate.
   * @param array $options
   *   Additional options.
   *
   * @command uwgen:contact
   * @aliases uwgco
   * @usage uwgen:contact, uwgen:contact 5, uwgco 5
   * @option media
   *   To include media or not.
   * @option options
   *   Number of blocks on layout builder page.
   */
  public function generateContentContact(int $items = 1, array $options = [
    'media' => UWNodeContentServiceInterface::MEDIA_REQUIRED_SHARED,
    'blocks' => 3,
  ]): void {

    // Check if media has been created prior.
    if (empty($this->mediaApi->allGeneratedMedia())) {
      $this->logger()->warning('No media detected. Creating media entities now.');
      $this->mediaApi->createAllMedia();
    }

    if ($items > 0 && $items <= self::MAX_ENTITIES) {
      for ($i = 0; $i < $items; $i++) {
        $this->generator->generateContact($options);
      }

      $this->logger()->success(dt('Generated @num contact(s).', ['@num' => $items]));
    }
    else {
      $this->logger()->error(dt('Please provide positive int argument not greater than @max for numbers of contact to generate.', ['@max' => self::MAX_ENTITIES]));
    }
  }

  /**
   * Drush command to generate profile.
   *
   * @param int $items
   *   Number of profile to generate.
   * @param array $options
   *   Additional options.
   *
   * @command uwgen:profile
   * @aliases uwgp
   * @usage uwgen:profile, uwgen:profile 5, uwgp 5
   * @option media
   *   To include media or not.
   * @option options
   *   Number of blocks on layout builder page.
   */
  public function generateContentProfile(int $items = 1, array $options = [
    'media' => UWNodeContentServiceInterface::MEDIA_REQUIRED_SHARED,
    'blocks' => 3,
  ]): void {

    // Check if media has been created prior.
    if (empty($this->mediaApi->allGeneratedMedia())) {
      $this->logger()->warning('No media detected. Creating media entities now.');
      $this->mediaApi->createAllMedia();
    }

    if ($items > 0 && $items <= self::MAX_ENTITIES) {
      for ($i = 0; $i < $items; $i++) {
        $this->generator->generateProfile($options);
      }

      $this->logger()->success(dt('Generated @num profile(s).', ['@num' => $items]));
    }
    else {
      $this->logger()->error(dt('Please provide positive int argument not greater than @max for numbers of blog post to generate.', ['@max' => self::MAX_ENTITIES]));
    }
  }

  /**
   * Drush command to generate demo content.
   *
   * @command uwgen:demo
   * @aliases uwgdemo
   * @usage uwgen:demo, uwgdemo
   *
   * @throws \Exception
   */
  public function generateContentDemo(): void {

    // Check if media has been created prior.
    if (empty($this->mediaApi->allGeneratedMedia())) {
      $this->logger()->warning('No media detected. Creating media entities now.');
      $this->mediaApi->createAllMedia();
    }

    // Generate blogs.
    for ($i = 0; $i < random_int(1, self::MAX_ENTITIES); $i++) {
      $this->generator->generateBlog($this->randomizeOptions());
    }
    $this->logger()->success(dt('Successfully generated @num blog(s).', ['@num' => $i]));

    // Generate contacts.
    for ($i = 0; $i < random_int(1, self::MAX_ENTITIES); $i++) {
      $this->generator->generateContact($this->randomizeOptions());
    }
    $this->logger()->success(dt('Successfully generated @num contact(s).', ['@num' => $i]));

    // Generate catalogs.
    $this->generateCatalogs();
    for ($i = 0; $i < random_int(1, self::MAX_ENTITIES); $i++) {
      $this->generator->generateCatalogItem($this->randomizeOptions());
    }
    $this->logger()->success(dt('Successfully generated @num catalog item(s).', ['@num' => $i]));

    // Generate events.
    for ($i = 0; $i < random_int(1, self::MAX_ENTITIES); $i++) {
      $this->generator->generateEvent($this->randomizeOptions());
    }
    $this->logger()->success(dt('Successfully generated @num event(s).', ['@num' => $i]));

    // Generate news items.
    for ($i = 0; $i < random_int(1, self::MAX_ENTITIES); $i++) {
      $this->generator->generateNewsItem($this->randomizeOptions());
    }
    $this->logger()->success(dt('Successfully generated @num news item(s).', ['@num' => $i]));

    // Generate profiles.
    for ($i = 0; $i < random_int(1, self::MAX_ENTITIES); $i++) {
      $this->generator->generateProfile($this->randomizeOptions());
    }
    $this->logger()->success(dt('Successfully generated @num profile(s).', ['@num' => $i]));

    // Generate projects.
    for ($i = 0; $i < random_int(1, self::MAX_ENTITIES); $i++) {
      $this->generator->generateProject($this->randomizeOptions());
    }
    $this->logger()->success(dt('Successfully generated @num project(s).', ['@num' => $i]));

    // Generate opportunities.
    for ($i = 0; $i < random_int(1, self::MAX_ENTITIES); $i++) {
      $this->generator->generateOpportunity($this->randomizeOptions());
    }
    $this->logger()->success(dt('Successfully generated @num opportunities.', ['@num' => $i]));

    // Generate services.
    $this->generateServiceCategories();
    for ($i = 0; $i < random_int(1, self::MAX_ENTITIES); $i++) {
      $this->generator->generateService($this->randomizeOptions());
    }
    $this->logger()->success(dt('Successfully generated @num service(s).', ['@num' => $i]));

    // Generator a site footer, if there isn't one already.
    if ($this->generator->checkSiteFooter()) {
      $this->logger()->warning(dt('There is already a site footer, will not generate.'));
    }
    else {
      $options['blocks'] = 1;
      $this->generator->generateSiteFooter($options);
      $this->logger()->success(dt('Successfully generated site footer.'));
    }

    // Generate web pages.
    for ($i = 0; $i < random_int(1, self::MAX_ENTITIES); $i++) {
      $this->generator->generateWebpage($this->randomizeOptions());
    }
    $this->logger()->success(dt('Successfully generated @num webpage(s).', ['@num' => $i]));

    // Get all the main menu links.
    $menu_links = $this->entityTypeManager->getStorage('menu_link_content')
      ->loadByProperties(['menu_name' => 'main']);

    // The menu links to enable.
    $menu_links_to_enable = [
      'Opportunities',
      'Projects',
      'Services',
    ];

    // Step through each of the menu links in the main
    // menu and enable if in array above.
    foreach ($menu_links as $menu_link) {
      if (in_array($menu_link->gettitle(), $menu_links_to_enable)) {
        $menu_link->set('enabled', 1);
        $menu_link->save();
      }
    }
    $this->logger()->success(dt('Successfully updated menu items.'));

    $this->logger()->success(dt('Generated demo content.'));

  }

  /**
   * Randomize options for demo content per content type.
   *
   * @return array
   *   Random options used to generate demo content.
   *
   * @throws \Exception
   */
  private function randomizeOptions(): array {
    return [
      'media' => random_int(1, 4),
      'days-before' => random_int(-100, 0),
      'days-after' => random_int(0, 100),
      'blocks' => random_int(1, 5),
    ];
  }

  /**
   * Drush command to generate Catalog items.
   *
   * @param int $num
   *   How many catalog items to generate.
   * @param bool $status
   *   Status published/unpublished for newly created catalog items.
   * @param array $options
   *   Additional options for catalog items.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   *
   * @command uwgen:catalog-items
   * @aliases uwgci
   * @usage uwgen:catalog-items 3 or uwgci 3
   */
  public function generateContentCatalogItems(int $num = 5, bool $status = TRUE, array $options = [
    'media' => UWNodeContentServiceInterface::MEDIA_OPTIONAL_SHARED,
    'days-before' => -100,
    'days-after' => 100,
    'blocks' => 3,
  ]): void {

    $catalogs = $this->entityTypeManager->getStorage('taxonomy_term')
      ->loadByProperties([
        'vid' => 'uw_vocab_catalogs',
      ]);

    // If there is no catalog (taxonomy term) exist, create random.
    if (!count($catalogs)) {
      $this->logger()->warning('No catalogs detected. Creating random catalogs.');
      $this->generateCatalogs();
    }

    if ($num > 0 && $num <= self::MAX_ENTITIES) {
      for ($i = 0; $i < $num; $i++) {
        $this->generator->generateCatalogItem($options, $status);
      }

      $this->logger()->success(dt('Generated @num catalog item(s).', ['@num' => $num]));
    }
    else {
      $this->logger()->error(dt('Please provide positive int argument not greater than @max for numbers of catalog items to generate.', ['@max' => self::MAX_ENTITIES]));
    }
  }

  /**
   * Drush command to generate services.
   *
   * @param int $num
   *   How many services to generate.
   * @param bool $status
   *   Status published/unpublished for newly created services.
   * @param array $options
   *   Additional options for services.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   *
   * @command uwgen:services
   * @aliases uwgs
   * @usage uwgen:services 3 or uwgs 3
   */
  public function generateServices(int $num = 5, bool $status = TRUE, array $options = [
    'media' => UWNodeContentServiceInterface::MEDIA_OPTIONAL_SHARED,
    'days-before' => -100,
    'days-after' => 100,
    'blocks' => 3,
  ]): void {

    // Check if media has been created prior.
    if (empty($this->mediaApi->allGeneratedMedia())) {
      $this->logger()->warning('No media detected. Creating media entities now.');
      $this->mediaApi->createAllMedia();
    }

    // Get the service categories from the taxonomy term.
    $service_categories = $this->entityTypeManager->getStorage('taxonomy_term')
      ->loadByProperties([
        'vid' => 'uw_vocab_service_categories',
      ]);

    // If there is no service categories (taxonomy term) exist, create random.
    if (!count($service_categories)) {
      $this->logger()->warning('No service categories detected. Creating random service categories.');
      $this->generateServiceCategories();
    }

    if ($num > 0 && $num <= self::MAX_ENTITIES) {
      for ($i = 0; $i < $num; $i++) {
        $this->generator->generateService($options, $status);
      }

      $this->logger()->success(dt('Generated @num service(s).', ['@num' => $num]));
    }
    else {
      $this->logger()->error(dt('Please provide positive int argument not greater than @max for numbers of catalog items to generate.', ['@max' => self::MAX_ENTITIES]));
    }
  }

  /**
   * Drush command to generate opportunity.
   *
   * @param int $num
   *   How many opportunity items to generate.
   * @param bool $status
   *   Status published/unpublished for newly created catalog items.
   * @param array $options
   *   Additional options for catalog items.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   *
   * @command uwgen:opportunity
   * @aliases uwgo
   * @usage uwgen:opportunity 3 or uwgo 3
   */
  public function generateContentOpportunity(int $num = 5, bool $status = TRUE, array $options = [
    'media' => UWNodeContentServiceInterface::MEDIA_OPTIONAL_SHARED,
    'days-before' => -100,
    'days-after' => 100,
    'blocks' => 3,
  ]): void {
    if ($num > 0 && $num <= self::MAX_ENTITIES) {
      for ($i = 0; $i < $num; $i++) {
        $this->generator->generateOpportunity($options, $status);
      }

      $this->logger()->success(dt('Generated @num opportunity item(s).', ['@num' => $num]));
    }
    else {
      $this->logger()->error(dt('Please provide positive int argument not greater than @max for numbers of opportunity items to generate.', ['@max' => self::MAX_ENTITIES]));
    }
  }

}
